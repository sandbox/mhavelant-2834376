<?php

namespace Drupal\google_adwords_api_integration\Api\v201609;

use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\PrivateStream;

/**
 * Class Credentials.
 *
 * @package Drupal\google_adwords\Api\Includes
 */
class AdWordsCredentials {

  private $basePath = NULL;
  private $authPath = NULL;
  private $settingsPath = NULL;
  private $clientCustomerId = NULL;
  private $developerToken = NULL;
  private $userAgent = NULL;
  private $clientId = NULL;
  private $clientSecret = NULL;
  private $refreshToken = NULL;
  private $valid = FALSE;
  private $errors = array();

  const SETTINGS_FOLDER_PATH = DIRECTORY_SEPARATOR . 'google_adwords_api_integration_config' . DIRECTORY_SEPARATOR;
  const AUTH_PATH = DIRECTORY_SEPARATOR . 'google_adwords_api_integration_config' . DIRECTORY_SEPARATOR . 'auth.ini';
  const SETTINGS_PATH = DIRECTORY_SEPARATOR . 'google_adwords_api_integration_config' . DIRECTORY_SEPARATOR . 'settings.ini';

  /**
   * AdWordsCredentials constructor.
   *
   * Creates the instance and loads the credentials from a specified source.
   * The list of sources (in precedence):
   *   admin UI // @todo: Not yet implemented.
   *   settings.php
   *   auth.ini
   * Whichever is found first will be loaded.
   *
   * @param string $authPath
   *   The path to the auth relative to the $basePath.
   * @param string $settingsPath
   *   The path to the settings relative to the $basePath.
   * @param string $basePath
   *   The path of the base folder without the trailing / .
   * @param string $customerClientID
   *   The AdWords Customer Client ID.
   */
  public function __construct(string $authPath = NULL, string $settingsPath = NULL, string $basePath = NULL, string $customerClientID = NULL) {
    $this->basePath = $basePath;
    if (NULL === $basePath) {
      $this->basePath = PrivateStream::basePath();
    }

    if (NULL === $authPath) {
      $this->setAuthPath($this->getBasePath() . $this::AUTH_PATH);
    }
    else {
      $this->setAuthPath($authPath);
    }

    if (NULL === $settingsPath) {
      $this->setSettingsPath($this->getBasePath() . $this::SETTINGS_PATH);
    }
    else {
      $this->setSettingsPath($settingsPath);
    }

    if (NULL !== $customerClientID) {
      $this->setClientCustomerId($customerClientID);
    }

    if (!$this->isValid()) {
      // Try to load credentials from settings.
      try {
        $this->loadFromSettings();
      }
      catch (\InvalidArgumentException $exception) {
        $this->addError($exception->getMessage());
      }
    }

    if (!$this->isValid()) {
      try {
        $this->loadFromFile();
      }
      catch (\InvalidArgumentException $exception) {
        $this->addError($exception->getMessage());
      }
    }

    if (!$this->isValid()) {
      throw new \InvalidArgumentException($this->errorsAsString());
    }
  }

  /**
   * Load credentials from file.
   *
   * @param string|null $authPath
   *   The absolute path to the auth.ini file.
   * @param string|null $settingsPath
   *   The absolute path to the settings.ini file.
   */
  public function loadFromFile(string $authPath = NULL, string $settingsPath = NULL) {
    if (NULL == $authPath) {
      $authPath = $this->getBasePath() . $this::AUTH_PATH;
    }

    if (NULL == $settingsPath) {
      $settingsPath = $this->getBasePath() . $this::SETTINGS_PATH;
    }

    if (empty($authPath) || !file_exists($authPath)) {
      throw new \InvalidArgumentException(
        t("The authentication file does not exist here: %path!", ["%path" => $authPath])
      );
    }

    if (($parsedIni = parse_ini_file($authPath, TRUE)) === FALSE) {
      throw new \InvalidArgumentException(
        t("The authentication cannot be parsed from here: %path", ["%path" => $authPath])
      );
    }

    if (empty($settingsPath) || !file_exists($settingsPath)) {
      throw new \InvalidArgumentException(
        t("The settings.ini file does not exist here: %path!", ["%path" => $settingsPath])
      );
    }

    $this->fromArray(
      [
        'authPath' => $authPath,
        'settingsPath' => $settingsPath,
      ] + $parsedIni
    );
  }

  /**
   * Convert the errors into a single string then clear the error array.
   *
   * @return string
   *   The errors as string.
   */
  private function errorsAsString() {
    $errorStr = '';

    foreach ($this->getErrors() as $error) {
      $errorStr .= ' ' . $error;
    }

    $this->setErrors(array());
    return trim($errorStr);
  }

  /**
   * Return the error array.
   *
   * @return array
   *   The errors.
   */
  public function getErrors() {
    return $this->errors;
  }

  /**
   * Add an error to the errors array.
   *
   * @param string $error
   *   The new error.
   */
  private function addError(string $error) {
    $this->errors[] = $error;
  }

  /**
   * Set the errors array.
   *
   * @param array $errors
   *   The new errors.
   */
  private function setErrors(array $errors) {
    $this->errors = $errors;
  }

  /**
   * Sets the $valid variable.
   */
  private function validate() {
    // If authFile is set and exists, the credentials are considered valid.
    if (NULL != $this->getAuthPath() && file_exists($this->getAuthPath())) {
      $this->valid = TRUE;
      return;
    }

    // Otherwise, we check if necessary data has been added.
    $this->valid = NULL != $this->getDeveloperToken() &&
     NULL != $this->getClientId() &&
     NULL != $this->getClientSecret() &&
     NULL != $this->getRefreshToken();
  }

  /**
   * Loads credentials from the project settings.php.
   *
   * @param string $settingKey
   *   The key from which to load.
   *
   * @throws \InvalidArgumentException
   *   Thrown when the supplied settings key is not found.
   *
   * @return $this
   */
  public function loadFromSettings(string $settingKey = 'google_adwords') {
    $apiSettings = Settings::get($settingKey);

    if (empty($apiSettings)) {
      throw new \InvalidArgumentException(
        t("The supplied key (%key) doesn't exist in your settings.php!", ["%key" => $settingKey])
      );
    }

    return $this->fromArray($apiSettings);
  }

  /**
   * Populates the object instance from an array.
   *
   * @param array $credentials
   *   The array with credentials.
   *
   * @return $this
   *   The credentials object.
   */
  public function fromArray(array $credentials) {
    $this->normalizeCredentialsSource($credentials);

    $this->setSettingsPath($credentials['settingsPath']);
    $this->setAuthPath($credentials['authPath']);

    $this->setDeveloperToken($credentials['developerToken']);
    if (NULL !== $credentials['clientCustomerId']) {
      $this->setClientCustomerId($credentials['clientCustomerId']);
    }
    $this->setUserAgent($credentials['userAgent']);

    $this->setClientId($credentials['OAUTH2']['client_id']);
    $this->setClientSecret($credentials['OAUTH2']['client_secret']);
    $this->setRefreshToken($credentials['OAUTH2']['refresh_token']);

    $this->validate();

    return $this;
  }

  /**
   * We make sure every array key is initialized.
   *
   * @param array $credentials
   *   The credentials which we normalize.
   */
  private function normalizeCredentialsSource(array &$credentials) {
    $arrayKeys = [
      'authPath',
      'settingsPath',
      'clientCustomerId',
      'userAgent',
      'OAUTH2',
    ];

    $oAuthKeys = [
      'client_id',
      'client_secret',
      'refresh_token',
    ];

    foreach ($arrayKeys as $key) {
      if (!isset($credentials[$key])) {
        $credentials[$key] = NULL;
      }
    }

    foreach ($oAuthKeys as $key) {
      if (!isset($credentials['OAUTH2'][$key])) {
        $credentials['OAUTH2'][$key] = NULL;
      }
    }
  }

  /**
   * Returns credentials as an array.
   *
   * @return array
   *   The credential array.
   */
  public function toArray() {
    return [
      'authPath' => $this->getAuthPath(),
      'settingsPath' => $this->getSettingsPath(),
      'developerToken' => $this->getDeveloperToken(),
      'clientCustomerId' => $this->getClientCustomerId(),
      'userAgent' => $this->getUserAgent(),
      'OAUTH2' => [
        'client_id' => $this->getClientId(),
        'client_secret' => $this->getClientSecret(),
        'refresh_token' => $this->getRefreshToken(),
      ],
    ];
  }

  /**
   * Sets the Authentication Path.
   *
   * @param string $authPath
   *   The path of the AdWords auth.ini.
   */
  private function setAuthPath($authPath) {
    $this->authPath = $authPath;
  }

  /**
   * Sets the path of the settings.ini.
   *
   * @param string $settingsPath
   *   The path of the settings.ini.
   */
  private function setSettingsPath($settingsPath) {
    $this->settingsPath = $settingsPath;
  }

  /**
   * Sets the Client Customer ID.
   *
   * @param string $clientCustomerId
   *   The Client Customer ID.
   */
  public function setClientCustomerId($clientCustomerId) {
    $this->clientCustomerId = $clientCustomerId;
  }

  /**
   * Sets the Client ID.
   *
   * @param string $clientId
   *   The OAuth Client ID.
   */
  private function setClientId($clientId) {
    $this->clientId = $clientId;
  }

  /**
   * Sets the Client Secret.
   *
   * @param string $clientSecret
   *   The OAuth Client Secret.
   */
  private function setClientSecret($clientSecret) {
    $this->clientSecret = $clientSecret;
  }

  /**
   * Sets the Refresh Token.
   *
   * @param string $refreshToken
   *   The OAuth Refresh Token.
   */
  private function setRefreshToken($refreshToken) {
    $this->refreshToken = $refreshToken;
  }

  /**
   * Sets the Developer Token.
   *
   * @param string $developerToken
   *   The Developer Token.
   */
  private function setDeveloperToken($developerToken) {
    $this->developerToken = $developerToken;
  }

  /**
   * Sets the User Agent.
   *
   * @param string $userAgent
   *   The User Agent.
   */
  private function setUserAgent($userAgent) {
    $this->userAgent = $userAgent;
  }

  /**
   * Returns the base path for the config files.
   *
   * @return string
   *   The base path.
   */
  private function getBasePath() {
    return $this->basePath;
  }

  /**
   * Returns the Authentication Path.
   *
   * @return mixed
   *   The path to the AdWords auth.ini.
   */
  public function getAuthPath() {
    return $this->authPath;
  }

  /**
   * Returns the Settings Path.
   *
   * @return mixed
   *   The path to the AdWords settings.ini.
   */
  public function getSettingsPath() {
    return $this->settingsPath;
  }

  /**
   * Returns the Client Customer ID.
   *
   * @return mixed
   *   The Client Customer ID.
   */
  public function getClientCustomerId() {
    return $this->clientCustomerId;
  }

  /**
   * Returns the Developer Token.
   *
   * @return mixed
   *   The Developer Token.
   */
  public function getDeveloperToken() {
    return $this->developerToken;
  }

  /**
   * Returns the User Agent.
   *
   * @return mixed
   *   The User Agent.
   */
  public function getUserAgent() {
    return $this->userAgent;
  }

  /**
   * Returns the Client ID.
   *
   * @return mixed
   *   The OAuth2 Client ID.
   */
  public function getClientId() {
    return $this->clientId;
  }

  /**
   * Returns the Client Secret.
   *
   * @return mixed
   *   The OAuth2 Client Secret.
   */
  public function getClientSecret() {
    return $this->clientSecret;
  }

  /**
   * Returns the Refresh Token.
   *
   * @return mixed
   *   The OAuth2 Refresh Token.
   */
  public function getRefreshToken() {
    return $this->refreshToken;
  }

  /**
   * Returns whether the credentials could be loaded and are valid.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isValid() {
    return $this->valid;
  }

  /**
   * Returns the OAuth2 related credentials.
   *
   * @return array
   *   The OAuth related credentials.
   */
  public function getOauth2Info() {
    return [
      'client_id' => $this->getClientId(),
      'client_secret' => $this->getClientSecret(),
      'refresh_token' => $this->getRefreshToken(),
    ];
  }

}
