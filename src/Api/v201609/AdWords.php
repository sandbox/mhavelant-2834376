<?php

namespace Drupal\google_adwords_api_integration\Api\v201609;

use AdWordsConstants;
use AdWordsUser;
use DateRange;
use OrderBy;
use Paging;
use Predicate;
use ReportDefinition;
use ReportUtils;
use Selector;

/**
 * Class AdWords.
 *
 * @package Drupal\google_adwords\Api\AdWords
 */
class AdWords {

  /**
   * The version of the GoogleAds PHP Lib used by this module.
   *
   * @var string
   */
  private $adWordsVersion = 'v201609';

  /**
   * The user.
   *
   * @var AdWordsUser
   */
  private $adWordsUser = NULL;

  /**
   * Credentials for the AdWords API.
   *
   * @var AdWordsCredentials
   */
  private $credentials = NULL;

  /**
   * Field selector object.
   *
   * @var Selector
   */
  private $selector = NULL;

  /**
   * An array of fields to select.
   *
   * @var string[]
   */
  private $selectorFields = NULL;

  /**
   * The Ordering to be used by the selector.
   *
   * @var OrderBy[]
   */
  private $selectorOrderBy = NULL;

  /**
   * The pagination to be used for the selection.
   *
   * @var Paging
   */
  private $selectorPaging = NULL;

  /**
   * The date range of the selection.
   *
   * @var DateRange
   */
  private $selectorDateRange = NULL;

  /**
   * The predicate of the selection.
   *
   * @var Predicate[]
   */
  private $selectorPredicate = NULL;

  /**
   * AdWords constructor.
   *
   * @param string $authPath
   *   The path to the auth relative to the $basePath.
   * @param string $settingsPath
   *   The path to the settings relative to the $basePath.
   * @param string $basePath
   *   The base path of the configs without the trailing / .
   * @param string $customerClientID
   *   The AdWords Customer Client ID.
   *
   * @throws \InvalidArgumentException
   */
  public function __construct(string $authPath = NULL, string $settingsPath = NULL, string $basePath = NULL, string $customerClientID = NULL) {
    $this->credentials = new AdWordsCredentials($authPath, $settingsPath, $basePath, $customerClientID);
    $this->setAdWordsUser();
  }

  /**
   * Sets the AdWords User according to the AdWordsCredentials object.
   */
  private function setAdWordsUser() {
    $this->adWordsUser = new AdWordsUser(
      $this->credentials->getAuthPath(),
      $this->credentials->getDeveloperToken(),
      $this->credentials->getUserAgent(),
      $this->credentials->getClientCustomerId(),
      $this->credentials->getSettingsPath(),
      $this->credentials->getOauth2Info()
    );
  }

  /**
   * Set the Client Customer ID for the user and also for the credentials.
   *
   * @param string $clientCustomerId
   *   The Client Customer ID.
   */
  public function setCustomerClientId(string $clientCustomerId) {
    $this->credentials->setClientCustomerId($clientCustomerId);
    $this->adWordsUser->SetClientCustomerId($clientCustomerId);
  }

  /**
   * Add a field to select.
   *
   * @param string $selectorField
   *   Name of the field.
   */
  public function addSelectorField(string $selectorField) {
    $this->selectorFields[] = $selectorField;
  }

  /**
   * Initializes the selector.
   *
   * The function uses these class-members:
   *   - Fields
   *   - OrderBy
   *   - Paging
   *   - DateRange
   *   - Predicates
   * Note, fields is required.
   */
  public function initSelector() {
    // @todo: Reports and Campaigns are different selectors, solve that..
    $this->selector = new Selector();

    if (empty($this->getSelectorFields())) {
      throw new \InvalidArgumentException(t('Fields are required to create a Selector instance.'));
    }

    $this->selector->fields = $this->getSelectorFields();

    if (!empty($this->getSelectorOrderBy())) {
      $this->selector->ordering = $this->getSelectorOrderBy();
    }

    if (!empty($this->getSelectorPaging())) {
      $this->selector->paging = $this->getSelectorPaging();
    }

    if (!empty($this->getSelectorDateRange())) {
      $this->selector->dateRange = $this->getSelectorDateRange();
    }

    if (!empty($this->getSelectorPredicate())) {
      $this->selector->predicates = $this->getSelectorPredicate();
    }

    return $this->selector;
  }

  /**
   * Hack until proper fix.
   */
  public function resetSelectordata() {
    // @todo: Reports and Campaigns are different selectors, solve that..
    $this->selectorPredicate = NULL;
    $this->selectorOrderBy = NULL;
    $this->selectorDateRange = NULL;
    $this->selectorPaging = NULL;
    $this->selectorFields = NULL;
  }

  /**
   * Returns the Fields for use in the Selector.
   *
   * @return string[]
   *   The array of fields to be selected.
   */
  public function getSelectorFields() {
    return $this->selectorFields;
  }

  /**
   * Set the fields to select.
   *
   * @param string[] $selectorFields
   *   The array of fields to select.
   */
  public function setSelectorFields(array $selectorFields) {
    $this->selectorFields = $selectorFields;
  }

  /**
   * Returns the OrderBy object.
   *
   * @return OrderBy[]
   *   The Order object.
   */
  public function getSelectorOrderBy() {
    return $this->selectorOrderBy;
  }

  /**
   * Set the ordering.
   *
   * @param array[] $orderingList
   *   An array of arrays. Inner array structure is this:
   *   [
   *     'field_name' => string,
   *     'order' => string
   *   ]
   *   If field_name is not found, that element is skipped.
   */
  public function setSelectorOrderBy(array $orderingList) {
    foreach ($orderingList as $ordering) {
      if (!isset($ordering['field_name'])) {
        continue;
      }

      $this->addSelectorOrderBy($ordering['field_name'], $ordering['order']);
    }
  }

  /**
   * Add an ordering.
   *
   * @param string|null $field
   *   The name of the field.
   * @param string|null $order
   *   The sorting order, ASCENDING or DESCENDING.
   */
  public function addSelectorOrderBy(string $field = NULL, string $order = NULL) {
    $this->selectorOrderBy[] = new OrderBy($field, $order);
  }

  /**
   * Returns the Paging object.
   *
   * @return Paging
   *   The Paging object.
   */
  public function getSelectorPaging() {
    return $this->selectorPaging;
  }

  /**
   * Set the Paging settings.
   *
   * @param int $startIndex
   *   The index of the first element to be returned.
   * @param int $resultCount
   *   The amount of data to be returned.
   */
  public function setSelectorPaging(int $startIndex = 0, int $resultCount = AdWordsConstants::RECOMMENDED_PAGE_SIZE) {
    $this->selectorPaging = new Paging($startIndex, $resultCount);
  }

  /**
   * Returns the DateRange object.
   *
   * @return DateRange
   *   The DateRange object.
   */
  public function getSelectorDateRange() {
    return $this->selectorDateRange;
  }

  /**
   * Set the range.
   *
   * @param string|null $from
   *   YYYYMMDD formatted string, the start of the range.
   * @param string|null $to
   *   YYYYMMDD formatted string, the end of the range.
   */
  public function setSelectorDateRange(string $from = NULL, string $to = NULL) {
    $this->selectorDateRange = new DateRange($from, $to);
  }

  /**
   * Returns the Predicate object.
   *
   * @return Predicate[]
   *   The Predicate object.
   */
  public function getSelectorPredicate() {
    return $this->selectorPredicate;
  }

  /**
   * Set the Predicate.
   *
   * @param array[] $predicateList
   *   An array of arrays. Inner array structure is this:
   *   [
   *     'field_name' => string,
   *     'operator' => string,
   *     'values' => array
   *   ]
   *   If field_name is not found, that element is skipped.
   */
  public function setSelectorPredicate(array $predicateList) {
    foreach ($predicateList as $predicate) {
      if (!isset($predicate['field_name']) || !isset($predicate['operator'])) {
        continue;
      }

      $this->addSelectorPredicate(
        $predicate['field_name'],
        $predicate['operator'],
        $predicate['values']
      );
    }
  }

  /**
   * Add a predicate to the selector.
   *
   * @param string $field
   *   Name of the field.
   * @param string $operator
   *   The predicate. Can be IN, NOT_IN, EQUALS, CONTAINS_ANY, etc.
   *   For more, look at the GoogleAds PHP Lib.
   * @param string[] $values
   *   Array of values.
   */
  public function addSelectorPredicate(string $field = NULL, string $operator = NULL, array $values = NULL) {
    $this->selectorPredicate[] = new Predicate($field, $operator, $values);
  }

  /**
   * Returns the Selector instance.
   *
   * @return \Selector
   *   The selector instance.
   */
  public function getSelector() {
    return $this->selector;
  }

  /**
   * Returns campaigns for the current configuration (user + selector).
   *
   * @return mixed
   *   The entries.
   */
  public function getCampaigns() {
    $this->initSelector();
    // Get the service.
    $campaignService = $this->adWordsUser->GetService('CampaignService', $this->adWordsVersion);

    // Make the get request.
    return $campaignService->get($this->getSelector())->entries;
  }

  /**
   * Get the report list.
   *
   * Note, this function downloads the reports as XML and then parses that
   * to an array.
   *
   * @return array
   *   The reports.
   */
  public function getReports() {
    $this->initSelector();

    // Create report definition.
    $reportDefinition = new ReportDefinition();
    $reportDefinition->selector = $this->getSelector();
    $reportDefinition->reportName = 'Criteria performance report';
    $reportDefinition->dateRangeType = 'ALL_TIME';
    $reportDefinition->reportType = 'CRITERIA_PERFORMANCE_REPORT';
    $reportDefinition->downloadFormat = 'XML';

    $reportUtils = new ReportUtils();
    $report = $reportUtils->DownloadReport($reportDefinition, NULL, $this->adWordsUser);

    /** @var \SimpleXMLElement $report */
    $report = simplexml_load_string($report);

    $reportList = array();
    /** @var \SimpleXMLElement $row */
    foreach ($report->table->row as $row) {
      $intermediateArray = array();
      foreach ($row->attributes() as $name => $value) {
        // @fixme
        $intermediateArray[$name] = (string) $value;
      }
      $reportList[] = $intermediateArray;
    }

    return $reportList;
  }

}
