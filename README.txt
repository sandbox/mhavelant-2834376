Pre-requisites
--------------
Use "composer require googleads/googleads-php-lib:^14.0" and add the class map
from the composer.json file of this module to the composer.json file of your
project.

Add this to the autoload/classmap in your composer.json:
"vendor/googleads/googleads-php-lib/src/Google/Api/Ads/AdWords/Util/v201609"

The module uses the googleads php lib which has its own requirements.
Check the following link:
https://github.com/googleads/googleads-php-lib/blob/master/README.md

Credentials
--------------
This is the precedence of the credential-checks
1, Settings.php: The app looks for this array.
Note: The 'google_adwords' key is configurable, you can use something else.

$settings['google_adwords'] = [
  'developerToken' => '',
  'userAgent' => '',
  'clientCustomerId' => '',
  'OAUTH2' => [
      'client_id' => '',
      'client_secret' => '',
      'refresh_token' => '',
  ],
];

2, The PHP Lib's config files:
- Files are stored in the private file system / google_adwords_api_integration_config folder.
- They are copied to there during install.
- Edit the auth.ini file and fill it with your credentials.
- Edit or delete the settings.ini file.

For more info see:
https://github.com/googleads/googleads-php-lib/wiki/API-access-using-own-credentials-(installed-application-flow)

Additional
--------------
https://developers.google.com/adwords/api/docs/guides/first-api-call
